<?php

/*

@package wordpress102
    ===================
        ADMIN PAGE
    ===================
 */

function sunset_add_admin_page() {

    //Generate Sunset Admin Page
    add_menu_page( 'Sunset Theme Options', 'Sunset', 'manage_options', 'wuyuan_sunset', 'sunset_theme_create_page', get_template_directory_uri() . '/img/sunset-icon.png', 110 );

    //Generate Sunset Admin Sub Pages
    add_submenu_page('wuyuan_sunset', 'Sunset Theme Options', 'General', 'manage_options', 'wuyuan_sunset', 'sunset_theme_create_page');

    add_submenu_page('wuyuan_sunset', 'Sunset CSS Options', 'Custom CSS', 'manage_options', 'wuyuan_sunset_css', 'sunset_theme_settings_page');
}

add_action( 'admin_menu', 'sunset_add_admin_page' );

function sunset_theme_create_page() {
    //generation of our admin page
    require_once( get_template_directory() . '/inc/templates/sunset-admin.php');
}

function sunset_theme_settings_page() {
    //generation of our admin page
}